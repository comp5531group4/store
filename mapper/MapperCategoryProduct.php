<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAbstract.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/CategoryProduct.php');

class MapperCategoryProduct extends MapperAbstract
{
	private $tableName;

	public function __construct()
    {
        parent::__construct();
        $this->tableName = 'Category_Product';
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    protected function create()
    {
        return new CategoryProduct();
    }

    protected function populate(AbstractObject $obj, $data)
    {
        if ( $obj instanceof CategoryProduct ) 
        {
            $obj->setId( $data->category_product_id );
            $obj->setDescription( $data->description );
        }
		return $obj;
    }
}

?>
