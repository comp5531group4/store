<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAbstract.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Customer.php');

class MapperCustomer extends MapperAbstract
{
	private $tableName;

	public function __construct()
    {
        parent::__construct();
        $this->tableName = 'Customer';
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    protected function create()
    {
        return new Customer;
    }

    protected function populate(AbstractObject $obj, $data)
    {
        if ( $obj instanceof Customer ) 
        {
            $obj->setId( $data->customer_id );
            $obj->setAddress( $data->address );
            $obj->setFirstname( $data->firstname );
            $obj->setLastname( $data->lastname );
            $obj->setEmail( $data->email );
            $obj->setPhoneNumber( $data->phone_number );
            $obj->setCreateDate( $data->create_date );
        }
		return $obj;
    }
}

?>
