<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAbstract.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Service.php');

class MapperService extends MapperAbstract
{
	private $tableName;

	public function __construct()
    {
        parent::__construct();
        $this->tableName = 'Service';
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    protected function create()
    {
        return new Service;
    }

    protected function populate(AbstractObject $obj, $data)
    {
        if ( $obj instanceof Service ) 
        {
            $obj->setId( $data->service_id ); 
            $obj->setCategoryId( $data->category_id ); 
            $obj->setName( $data->name );
            $obj->setPrice( $data->price );
            $obj->setDescription( $data->description );
            $obj->setPicture( $data->picture );
        }
		return $obj;
    }
}

?>
