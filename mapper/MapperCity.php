<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAbstract.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/City.php');

class MapperCity extends MapperAbstract
{
	private $tableName;

	public function __construct()
    {
        parent::__construct();
        $this->tableName = 'City';
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    protected function create()
    {
        return new City;
    }

    protected function populate(AbstractObject $obj, $data)
    {
        if ( $obj instanceof City ) 
        {
            $obj->setId( $data->city_id );
            $obj->setStateId( $data->state_id );
            $obj->setName( $data->name );
        }
		return $obj;
    }
}

?>
