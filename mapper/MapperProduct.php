<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAbstract.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Product.php');

class MapperProduct extends MapperAbstract
{
	private $tableName;

	public function __construct()
    {
        parent::__construct();
        $this->tableName = 'Product';
    }

    protected function getTableName()
    {
        return $this->tableName;
    }

    protected function create()
    {
        return new Product;
    }

    protected function populate(AbstractObject $obj, $data)
    {
        if ( $obj instanceof Product ) 
        {
            $obj->setId( $data->product_id );           
            $obj->setCategoryId( $data->category_product_id );   
            $obj->setName( $data->name );          
            $obj->setDescription( $data->description );  
            $obj->setUnitsInStock( $data->units_in_stock ); 
            $obj->setUnitsOnOrder( $data->units_on_order ); 
            $obj->setPrice( $data->price );
            $obj->setPicture( $data->picture );      
        }
		return $obj;
    }

    public function getProductByCategory($category_product_id)
    {
        $q = 'SELECT * FROM Product WHERE category_product_id='.$category_product_id.';';

        $this->selectFreeRun($q);

        $mySqlObjects = $this->getObjects();
    
        return $this->getDerivedObjects($mySqlObjects);
    }

    
}

?>
