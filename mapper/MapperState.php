<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAbstract.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/State.php');

class MapperState extends MapperAbstract
{
	private $tableName;

	public function __construct()
    {
        parent::__construct();
        $this->tableName = 'State';
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    protected function create()
    {
        return new State;
    }

    protected function populate(AbstractObject $obj, $data)
    {
        if ( $obj instanceof State ) 
        {
            $obj->setId( $data->state_id );
            $obj->setCountryId( $data->country_id );
            $obj->setName( $data->name );
        }
		return $obj;
    }

    public function selectAllCitiesWhereState($state_id)
    {
        $q ="SELECT c.name, c.city_id FROM State s, City c WHERE c.state_id='$state_id' GROUP BY c.city_id;";

        $this->selectFreeRun($q);

        return $this->getObjects();
    }
}

?>
