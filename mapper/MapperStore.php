<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAbstract.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Store.php');

class MapperStore extends MapperAbstract
{
	private $tableName;

	public function __construct()
    {
        parent::__construct();
        $this->tableName = 'Store';
    }

    protected function getTableName()
    {
        return $this->tableName;
    }

    protected function create()
    {
        return new Store;
    }

    protected function populate(AbstractObject $obj, $data)
    {
        if ( $obj instanceof Store ) 
        {
            $obj->setId( $data->user_id );           
            $obj->setName( $data->name ); 
        }
		return $obj;
    }
}

?>
