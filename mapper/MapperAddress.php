<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAbstract.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Address.php');

class MapperAddress extends MapperAbstract
{
	private $tableName;

	public function __construct()
    {
        parent::__construct();
        $this->tableName = 'Address';
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    protected function create()
    {
        return new Address;
    }

    protected function populate(AbstractObject $obj, $data)
    {
        if ( $obj instanceof Address ) 
        {
            $obj->setId( $data->address_id );
            $obj->setCityId( $data->city_id );
            $obj->setStreet1( $data->street1 );
            $obj->setStreet2( $data->street2 );
            $obj->setPostalCode( $data->postal_code );
        }
		return $obj;
    }
}

?>
