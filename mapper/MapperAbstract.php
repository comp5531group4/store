<?php
/*
 * Extends mysql.php. 
 * Note: All other mapper class MUST extend this class.
 */
include_once($_SERVER['DOCUMENT_ROOT'].'/includes/MySql.php');

abstract class MapperAbstract extends MySql 
{
    protected $objects;

	public function __construct()
    {
        parent::__construct();
        $this->dbConnect();
        $objects = array();
    }    

    public function findById($id)
    {
        $tableName = $this->getTableName();
    
        $this->sqlQuery = 'SELECT * FROM '.$tableName.' WHERE '.strtolower( $tableName ).'_id='.$id.";";
        
        $this->sqlResult = $this->selectFreeRun( $this->sqlQuery );
        $mySqlObjects = $this->getObjects();

        $this->sqlQuery = NULL;

        $objectArray = $this->getDerivedObjects($mySqlObjects);

        return $objectArray[0];
    }

    public function selectAll()
    {
        $this->selectAllFromTable( $this->getTableName() );
        $mySqlObjects = $this->getObjects();
    
        return $this->getDerivedObjects($mySqlObjects);
    }

    public function selectWhere($where)
    {
        $this->selectFromTableWhere( $this->getTableName(), $where );
        $mySqlObjects = $this->getObjects();

        return $this->getDerivedObjects($mySqlObjects);   
    }

    public function insert(AbstractObject $obj) 
    {
        $this->insertInto( $this->getTableName(), $obj->toAssociativeArray() );
    }

    public function update(AbstractObject $obj)
    {
        $where = array(
                'row_name'=>strtolower( $this->getTableName() )."_id",
                'operator'=>'=',
                'value'=>$obj->getId(),
                'value_type'=>'integer'
        );

        $this->updateSetWhere( $this->getTableName(), $obj->toAssociativeArray(), $where );   
    }

    public function save(AbstractObject $obj)
    {
        if ( $obj->getId() == 0 ) {
            $this->insert($obj);
        } else {
            $this->update($obj);
        }
    }

    public function delete(AbstractObject $obj) 
    { 
        if ($obj && $obj->getId()) 
        {
            $tableName = $this->getTableName();
            $this->sqlQuery = "DELETE FROM ". $tableName ." WHERE ".$tableName."_id=".$obj->getId();
            
            $this->sqlResult = $this->freeRun($this->sqlQuery);
            $this->sqlQuery = NULL;
        
            return $this->sqlResult;
        }
    }

    /* 
     * Note: uses abstract method create() to get a derived object
     */
    protected function getDerivedObjects($mySqlObjects)
    {
        $objects = array();
        
        foreach ($mySqlObjects as $mySqlObj) {
            $objects[] = $this->populate($this->create(), $mySqlObj);
        }
        
        return $objects;
    }

    abstract protected function getTableName();
    abstract protected function create();
    abstract protected function populate(AbstractObject $obj, $data);
}
?>
