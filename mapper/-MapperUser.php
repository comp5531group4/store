<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAbstract.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/User.php');

class MapperUser extends MapperAbstract
{
	private $tableName;

	public function __construct()
    {
        parent::__construct();
        $this->tableName = 'User';
    }

    protected function getTableName()
    {
        return $this->tableName;
    }

    protected function create()
    {
        return new User;
    }

    protected function populate(AbstractObject $obj, $data)
    {
        if ( $obj instanceof User ) 
        {
            $obj->setId( $data->user_id );           
            $obj->setUsername( $data->username );          
            $obj->setPassword( $data->password ); 
        }
		return $obj;
    }
}

?>
