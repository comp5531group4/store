<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAbstract.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Country.php');

class MapperCountry extends MapperAbstract
{
	private $tableName;

	public function __construct()
    {
        parent::__construct();
        $this->tableName = 'Country';
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    protected function create()
    {
        return new Country;
    }

    protected function populate(AbstractObject $obj, $data)
    {
        if ( $obj instanceof Country ) 
        {
            $obj->setId( $data->country_id );
            $obj->setName( $data->name );
        }
		return $obj;
    }

    public function selectAllStatesWhereCountry($country_id)
    {
        $q ="SELECT s.name, s.state_id FROM Country c, State s WHERE s.country_id='$country_id' GROUP BY s.name;";

        $this->selectFreeRun($q);

        return $this->getObjects();
    }
}

?>
