<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAbstract.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Purchase.php');

class MapperPurchase extends MapperAbstract
{
	private $tableName;

	public function __construct()
    {
        parent::__construct();
        $this->tableName = 'Purchase';
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    protected function create()
    {
        return new Purchase;
    }

    protected function populate(AbstractObject $obj, $data)
    {
        if ( $obj instanceof Purchase ) 
        {
            $obj->setId( $data->purchase_id ); 
            $obj->setEmployeeId( $data->employee_id ); 
            $obj->setCustomerId( $data->customer_id ); 
            $obj->setStoreId( $data->store_id ); 
            $obj->setPurchaseDate( $data->purchase_date ); 
            $obj->setShipDate( $data->ship_date ); 
        }
		return $obj;
    }

    public function selectAllPurchaseProduct($purchase_id = 0)
    {
        $q = "SELECT 
                purchase_date as date, 
                p.name, 
                pp.unit_price, 
                pp.quantity, 
                pp.discount, 
                (pp.unit_price*pp.quantity-pp.discount) as total
            FROM 
                Purchase pr, 
                Purchase_Product pp, 
                Product p
            WHERE 
                pr.purchase_id=pp.purchase_id AND
                pp.product_id=p.product_id ";

        if ($purchase_id > 0) {
            $q .= "AND pr.purchase_id='$purchase_id' ";
        }

        $q .= "ORDER BY 
                date DESC, 
                name, 
                quantity DESC;";

        $this->selectFreeRun($q);

        return $this->getObjects();
    }

    public function selectAllPurchaseService($purchase_id = 0)
    {
        $q = "SELECT 
                purchase_date as date, 
                CONCAT(e.firstname, ' ', e.lastname) as employee_name, 
                s.name as service_name,
                ps.unit_price, 
                ps.quantity, 
                ps.discount,                
                (ps.unit_price*ps.quantity-ps.discount)*e.commission as commission, 
                (ps.unit_price*ps.quantity-ps.discount)*(1-(e.commission)) as revenue 
            FROM 
                Purchase pr, 
                Purchase_Service ps, 
                Service s,
                Employee e
            WHERE 
                e.employee_id=pr.employee_id AND
                pr.purchase_id=ps.purchase_id AND
                ps.service_id=s.service_id ";

        if ($purchase_id > 0) {
            $q .= "AND pr.purchase_id='$purchase_id' ";
        }

        $q .= "ORDER BY 
                date DESC, 
                name, 
                quantity DESC;";        

        $this->selectFreeRun($q);

        return $this->getObjects();
    }

    public function getPurchaseProductListByFrequency()
    {
        $q = "SELECT 
                p.name, 
                SUM(pp.quantity) as quantity,
                SUM( (pp.quantity*pp.unit_price-pp.discount)*(1-e.commission) ) as revenue
            FROM 
                Employee e,
                Purchase pr, 
                Product p, 
                Purchase_Product pp                
            WHERE 
                e.employee_id=pr.employee_id AND
                pr.purchase_id=pp.purchase_id AND
                pp.product_id=p.product_id
            GROUP BY 
                p.name
            ORDER BY 
                quantity DESC;";

        $this->selectFreeRun($q);

        return $this->getObjects();
    }

    // /view/history/repair.php
    public function selectAllPurchaseServiceFilterByCategory($category_id)
    {
        $q = "SELECT 
                purchase_date as date, 
                CONCAT(e.firstname, ' ', e.lastname) as employee_name, 
                CONCAT(c.firstname, ' ', c.lastname) as customer_name, 
                s.name as service_name,
                note
            FROM 
                Purchase_Service ps, 
                Category_Service cs, 
                Service s, 
                Employee e, 
                Purchase pr,
                Customer c
            WHERE
                c.customer_id=pr.customer_id AND
                e.employee_id=pr.employee_id AND
                pr.purchase_id=ps.purchase_id AND
                ps.service_id=s.service_id AND
                s.category_id=cs.category_id AND
                cs.category_id=$category_id
            ORDER BY 
                date DESC, employee_name, customer_name, service_name;";

        $this->selectFreeRun($q);

        return $this->getObjects();
    }

    public function selectAllPurchaseServiceWhereEmployeeIDDate($employee_id, $from_date, $to_date)
    {

        $q = "SELECT 
                pr.purchase_date as date, 
                CONCAT(e.firstname, ' ', e.lastname) as employee_name, 
                s.name as service_name,
                s.service_id,
                ps.unit_price, 
                ps.quantity, 
                ps.discount,                
                (ps.unit_price*ps.quantity-ps.discount)*e.commission as commission, 
                (ps.unit_price*ps.quantity-ps.discount)*(1-(e.commission)) as revenue 
            FROM 
                Purchase pr, 
                Purchase_Service ps, 
                Service s,
                Employee e
            WHERE 
                e.employee_id=pr.employee_id AND
                pr.purchase_id=ps.purchase_id AND
                ps.service_id=s.service_id AND
                pr.purchase_date BETWEEN '$from_date' AND '$to_date' ";

        if ( is_numeric($employee_id) && $employee_id > 0 ) {
            $q .= "AND e.employee_id=".$employee_id." ";
        }

        $q .= "ORDER BY 
            date DESC, 
            employee_name,
            service_name, 
            quantity DESC;";
      
        $this->selectFreeRun($q);

        return $this->getObjects();
    }

    public function getTotalRevenueByStore()
    {
        $q = "SELECT store_name, sum(sumcol) as revenue
            FROM (  
                SELECT 
                    st.name as store_name, 
                    (pp.unit_price*pp.quantity-pp.discount) as sumcol 
                FROM 
                    Purchase_Product pp, Store st, Purchase p
                WHERE 
                    st.store_id=p.store_id AND
                    p.purchase_id=pp.purchase_id

                UNION 

                SELECT 
                    st.name as store_name, 
                    (unit_price*quantity-discount) as sumcol
                FROM 
                    Purchase_Service ps, Store st, Purchase p
                WHERE 
                    st.store_id=p.store_id AND
                    p.purchase_id=ps.purchase_id
            ) as tmptable
            GROUP BY store_name;";

        $this->selectFreeRun($q);

        return $this->getObjects();
    }
}

?>
