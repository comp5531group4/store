<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAbstract.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Employee.php');

class MapperEmployee extends MapperAbstract
{
	private $tableName;

	public function __construct()
    {
        parent::__construct();
        $this->tableName = 'Employee';
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    protected function create()
    {
        return new Employee;
    }

    protected function populate(AbstractObject $obj, $data)
    {
        if ( $obj instanceof Employee ) 
        {
            $obj->setId( $data->employee_id );
            $obj->setAddressId( $data->address_id );
            $obj->setFirstname( $data->firstname );
            $obj->setLastname( $data->lastname );
            $obj->setCommission( $data->commission );
            $obj->setSeniority( $data->seniority );
            $obj->setPhoneNumber( $data->phone_number );
            $obj->setGender( $data->gender );
            $obj->setActive( $data->active );
            $obj->setHireDate( $data->hire_date );
        }
		return $obj;
    }   

    public function getPaymentHistoryWhereFrequencyEmployeeOrder($frequency, $employee, $order = array() )
    {
        $q = "SELECT YEAR(pr.purchase_date) as year, ";
        if ($frequency == 'Week') {   
            $q .= "WEEK(pr.purchase_date) as frequency, "; 
        }
        else if ($frequency == 'Month' ) {
            $q .= "MONTH(pr.purchase_date) as frequency, ";  
        }
        else if ($frequency == 'Year') {
            $q .= "YEAR(pr.purchase_date) as frequency, ";  
        }
        else
        {
            $q = ""; // should not happen!!
        } 

        $q .=   "e.employee_id, 
                CONCAT(e.firstname, ' ', e.lastname) as employee_name, 
                SUM( (pp.unit_price*pp.quantity-pp.discount)*e.commission ) as commission
            FROM 
                Purchase pr, 
                Purchase_Product pp, 
                Employee e
            WHERE
                e.employee_id=pr.employee_id AND
                pr.purchase_id=pp.purchase_id ";

        if ($employee instanceof Employee && $employee->getId() > 0)
        {
            $q .= " AND e.employee_id=".$employee->getId()." ";
        }

        $q .= "GROUP BY year, frequency, employee_id ORDER BY ";
        
        if ( !$order )
        {
            $q .= "year DESC, frequency DESC, employee_name;";
        }

        $this->selectFreeRun($q);

        return $this->getObjects();
    }
}
?>
