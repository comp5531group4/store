<?php
/*
 * MySql wrapper object.
 * 
 * Note: dbConnect() must be called before any 
 * queries can be performed. 
 * 
 * Ultimately all queries go through this class.
 *
 * Contains generic methods for 
 *   SELECT * FROM table_name
 *   SELECT * FROM table_name WHERE ...
 *   INSERT INTO table_name ...
 *   UPDATE table_name SET attribute1=value1, ... WHERE attribute_n operator value_n
 *
 * A method for any arbitrary MySqL query is also available. 
 */
include_once($_SERVER['DOCUMENT_ROOT'].'/config/DbConfig.php');

class MySql extends DbConfig
{
    protected $connectionString;
    protected $sqlQuery;
    protected $sqlResult;
    protected $objects;

    public $errors;
    public $messages;
    
    public function __construct()
    {
        parent::__construct();
        $this->connectionString = NULL;
        $this->sqlQuery         = NULL;
        $this->sqlResult        = NULL;
        $this->objects          = array();

        $this->errors           = array();
        $this->messages         = array();
    }

    /*
    * mysqli(server_name, user_name, password, database_name)
    */
    public function dbConnect()    
    {
        $this->connectionString = new mysqli($this->serverName, $this->username, $this->password, $this->dbName);
        if ( $this->connectionString->connect_errno ) {
            $this->errors[] = "Database connection problem.";
        } else {
            $this->messages[] = "Connection to database succesful";
        }
        return $this->connectionString;
    }

    public function dbDisconnect() 
    {
        $this->connectionString = NULL;
        $this->sqlQuery         = NULL;
        $this->sqlResult        = NULL;
        $this->objects          = NULL;
        $this->dbName           = NULL;
        $this->hostName         = NULL;
        $this->username         = NULL;
        $this->password         = NULL;
    }

    /*
    * SELECT * FROM table_name
    */
    public function selectAllFromTable($tableName)  
    {
        $this->sqlQuery = "SELECT * FROM ".$tableName;
        $this->sqlResult  = $this->connectionString->query($this->sqlQuery);

        if (!$this->sqlResult) {
            $this->errors[] = "Query failed: ".$this->sqlQuery;
            $this->errors[] = "Error: ".mysqli_error($this->connectionString);
        }
        $this->sqlQuery = NULL;
        
        return $this->sqlResult;    
    }

    /*
     * SELECT FROM table_name WHERE value_1 operator value_2
     * Note: 'operator' can be any of =, <, >, etc... 
     */
    public function selectFromTableWhere($tableName, $where)
    {
        $this->sqlQuery = "SELECT * FROM $tableName WHERE ";

        foreach ($where as $clause) 
        {
            if (count($clause) == 1) 
            {
                $this->sqlQuery .= $clause[0];
            }
            else 
            {
                $rowName    = $clause['row_name'];
                $operator   = $clause['operator'];
                $value      = $clause['value'];
                $valueType  = $clause['value_type'];

                $this->sqlQuery .= $rowName.$operator;

                if($valueType == 'string')   {
                    $this->sqlQuery .= "'".$value."'";
                } else {
                    $this->sqlQuery .= $value;
                }
            }
        }

        $this->sqlQuery .= ";";
        
        $this->sqlResult = $this->connectionString->query($this->sqlQuery);

        if (!$this->sqlResult) {
            $this->errors[] = "Query failed: ".$this->sqlQuery;
            $this->errors[] = "Error: ".mysqli_error($this->connectionString);
        }
        $this->sqlQuery = NULL;

        return $this->sqlResult;
    }
    
    /*
     * INSERT INTO table_name (column1, column2, ...) VALUE (value1, value2, ...)
     * 
     * must have the form:
     * $data[] = array(column_name, value, type)
     */
    public function insertInto($tableName, $data) 
    {
        $this->sqlQuery = 'INSERT INTO '.$tableName.' (';

        $size = count($data);
        $i = 0;
        $valueList = "";
        foreach ($data as $columns => $column) 
        {
            $columnName = $column[0];
            $value = $column[1];
            $type = $column[2];

            $this->sqlQuery .= $columnName;

            // differentiate between string and numbers
            if ($type == "string") {
                $valueList .= "'".$value."'";    
            } else {
                if ( !is_numeric($value) )  {
                    $value = 0;
                }
                $valueList .= $value;    
            }

            // add commas
            if ( $i < $size-1 ) 
            {
                $i++;
                $this->sqlQuery .=", ";
                $valueList .=", ";
            }
        }   
        
        $this->sqlQuery .= ') VALUES ('.$valueList.');';
        $this->sqlResult = $this->connectionString->query($this->sqlQuery);

        if (!$this->sqlResult) {
            $this->errors[] = "Query failed: ".$this->sqlQuery;
            $this->errors[] = "Error: ".mysqli_error($this->connectionString);
        } 
        $this->sqlQuery = NULL;

        return $this->sqlResult;
    }

    /*
     * must have the form:
     *      $data[] = array(column_name, value, type);
     *      $where[] = array(
     *              row_name=>row, 
     *              operator=>op,
     *              value=>val,
     *              value_type=>val_type
     *      );
     */
    public function updateSetWhere($tableName, $data, $where) 
    {
        $this->sqlQuery = 'UPDATE '.$tableName.' SET ';

        $size = count($data);
        $i = 0;
        // ATTRIBUTES
        foreach ($data as $columns => $column) 
        {
            $columnName = $column[0];
            $value = $column[1];
            $type = $column[2];

            $this->sqlQuery .= $columnName."=";

            // differentiate between string and numbers
            if ($type == "string") {
                $this->sqlQuery .= "'".$value."'";    
            } else {
                $this->sqlQuery .= $value;    
            }

            // add commas
            if ( $i < $size-1 ) 
            {
                $i++;
                $this->sqlQuery .=", ";
            }
        }   
        
        // WHERE clause
        $this->sqlQuery .= " WHERE ". $where['row_name'].$where['operator'];
        if ($where['value_type'] == "string") {
            $this->sqlQuery .= "'".$where['value']."'";    
        } else {
            $this->sqlQuery .= $where['value'];    
        }    
        $this->sqlQuery .=";";    

        $this->sqlResult = $this->connectionString->query($this->sqlQuery);

        if (!$this->sqlResult) {
            $this->errors[] = "Query failed: ".$this->sqlQuery;
            $this->errors[] = "Error: ".mysqli_error($this->connectionString);
        } 
        $this->sqlQuery = NULL;

        return $this->sqlResult;
    }

    /*
     * Performs an arbitrary mysql query.
     * Saves and returns the result. 
     */
    public function selectFreeRun($query)  
    {
        $this->sqlResult = $this->connectionString->query($query);

        if (!$this->sqlResult) {
            $this->errors[] = "Query failed: ".$query;
            $this->errors[] = "Error: ".mysqli_error($this->connectionString);
        }        
        $query = NULL;

        return $this->sqlResult;
    }

    /*
     * Returns the result of an arbitrary mysql query.
     */
    public function freeRun($query)    
    {
        return $this->connectionString->query($query);
    }

    /*
     * Returns the result of the last attempted query.
     */
    public function getSqlResult()
    {
        return $this->sqlResult;
    }

    /*
     * Fetches the objects from the mysql object 
     * and returns them in an array.
     */
    public function getObjects()
    {
        $this->objects = array();
        if ($this->sqlResult) {
            while( $obj = $this->sqlResult->fetch_object() ) {
                $this->objects[] = $obj;
            }
            return $this->objects;    
        } else {
            return array();
        }   
    }
}

?>
