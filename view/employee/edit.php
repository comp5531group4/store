<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperEmployee.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Employee.php');

include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAddress.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Address.php');

include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperCountry.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Country.php');

include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperState.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/State.php');

include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperCity.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/City.php');

include_once($_SERVER['DOCUMENT_ROOT'].'/object/FormValidator.php');

include_once($_SERVER['DOCUMENT_ROOT'].'/object/Login.php'); 

$login = new Login();

$mapperEmployee = new MapperEmployee();
$mapperCity = new MapperCity();
$mapperState = new MapperState();
$mapperCountry = new MapperCountry();

$mapperAddress = new MapperAddress();
$cityList = $mapperCity->selectAll();
$stateList = $mapperState->selectAll();
$countryList = $mapperCountry->selectAll();

if( isset($_GET['id'] ) 
    && is_numeric($_GET['id']) 
    && $_GET['id'] > 0 
  ) {
  $employee_id = $_GET['id'];
} else if ( isset($_POST['employee_id']) ) {
  $employee_id = $_POST['employee_id'];
} else {
  $employee_id  = 0;  
}

if ($employee_id > 0) 
{
  $employee = $mapperEmployee->findById($employee_id);  
  $mode = 'Edit';
  $country_id = $employee->getAddress()->getCity()->getState()->getCountryId();
  $state_id   = $employee->getAddress()->getCity()->getStateId();
  $city_id    = $employee->getAddress()->getCityId();
} 
else 
{
  $employee = new Employee();
  $mode = 'Add';
  $country_id = 0;
  $state_id = 0;
  $city_id= 0;
}

// form fields to validate
$validations = array(
    'firstname'=>'anything',
    'lastname'=>'anything',
    'commission'=>'alfanum',
    //'seniority'=>'anything',
    'phone_number'=>'phone',        
    //'gender'=>'anything',
    //'active'=>'number',
    'hire_date'=>'date',

    'street1'=>'anything',
    'postal_code'=>'postal_code',
    'city_id' => 'nonzero',
    'state_id' => 'nonzero',
    'country_id' => 'nonzero'
  );
$required = array('firstname', 'lastname', 'street1', 'postal_code');
$sanatize = array();
$validator = array();

if($_POST) 
{
  // save form values
  $employee_id  = $_POST['employee_id'];
  $address_id   = $_POST['address_id'];

  $firstname    = $_POST['firstname'];
  $lastname     = $_POST['lastname'];
  $commission   = $_POST['commission'];
  $seniority    = $_POST['seniority'];
  $phone_number = $_POST['phone_number'];
  $gender       = $_POST['gender'];
  $active       = $_POST['active'];
  $hire_date    = $_POST['hire_date'];

  $stree1       = $_POST['street1'];
  $stree2       = $_POST['street1'];
  $postal_code  = $_POST['postal_code'];
  $city_id      = $_POST['city_id'];
  $state_id     = $_POST['state_id'];
  $country_id   = $_POST['country_id'];
  
  if ($employee_id > 0) {
    $employee = $mapperEmployee->findById($employee_id);  
  } else {
    $employee = new Employee();
  }

  // form validation
  $validator = new FormValidator($validations, $required, $sanatize);
  if( $employee && $validator->validate($_POST) && $mode = 'Edit' )
  {
    // SET ADDRESS
    $employeeAddress = $employee->getAddress();
    $employeeAddress->setStreet1($stree1);
    $employeeAddress->setStreet2($stree2);
    $employeeAddress->setPostalCode($postal_code);
    $employeeAddress->setCityId($city_id);
    
    // UPDATE ADDRESS
    $mapperAddress->update($employeeAddress);

    // SET EMPLOYEE DATA
    //$employee->setId($employee_id);
    //$employee->setAddressId($address_id);

    $employee->setFirstname($firstname);
    $employee->setLastname($lastname);
    $employee->setCommission($commission);
    $employee->setSeniority($seniority);
    $employee->setPhoneNumber($phone_number);
    $employee->setGender($gender);
    $employee->setActive($active);
    $employee->setHireDate($hire_date);
    
    // UPDATE EMPLOYEE
    $mapperEmployee->update($employee);
    
    // redirect 
    if($mapperEmployee->getSqlResult()) 
    { 
      header("Location: information.php?firstname=".$firstname."&lastname=".$lastname."&update=1"); 
    }

  } 
  else
  {
    foreach ($validator->errors as $key => $error) {
      echo '<br />Error. Field '.$key.' should be of type: '.$error.'<br />';
    }
  }
 
} 

?>

<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html style="" class=" js no-touch svg inlinesvg svgclippaths no-ie8compat" lang="en"><!--<![endif]--><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width">

  <title><?php echo $mode;?> Employee Information</title>

  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet">

  <script src="/js/custom.js"></script>

  <script src="/js/jquery.min.js"></script>
  <script src="/js/jquery-ui-1.10.3.custom.min.js"></script>
  <script src="/js/getCountryStateCityList.js"></script>

  <script>
    $(function() {
      $( ".datepicker" ).datepicker( { dateFormat: "yy-mm-dd" } );
    }); 
  </script>

</head>
<body>

<!-- Header and Nav -->

  <div class="row">
    <div class="large-3 columns">
      <h1><img src="/images/400x100textLogo.png"></h1>
    </div>

    <div class="large-7 columns">
      <ul class="inline-list right">
      <?php if (!$login->isUserLoggedIn) { ?>
        <li><a href="/view/login/index.php">Sign In</a></li>
      <?php } else { ?>
        <li><?php echo $login->messages[0]; ?></li>
        <li><a href="/view/login/index.php?logout=1">Logout</a></li>
      <?php } ?>
      </ul>
    </div>

    <div class="large-10s pull-2 columns">
      <ul class="inline-list right">
        <li><a href="/index.php" >Home</a></li>
        <li><a href="/view/history/repair.php" >View Repair History</a></li>
        <li><a href="/view/product/index.php" >Check Inventory</a></li>
      </ul>
    </div>
  </div>

  <!-- End Header and Nav -->
  
  <div class="row">    
    
    <!-- Main Content Section -->
    <!-- This has been source ordered to come first in the markup (and on small devices) but to be to the right of the nav on larger screens -->
    <div class="large-8 push-2 columns">
      
      <h3><?php echo $mode;?> Employee Information 
        <small><?php if ($employee_id > 0) { echo $employee->getFirstname().' '.$employee->getLastname();} ?>
        </small>
      </h3>
     <form action="edit.php" method="post">
      <div><input type="hidden" name="employee_id" value="<?php echo $employee->getId()?>"></div>
      <div><input type="hidden" name="address_id" value="<?php echo $employee->getAddressId()?>"></div>
      <table>
        <tr>
          <td>firstname</td>
          <td><input type="text" name="firstname" value="<?php echo $employee->getFirstname(); ?>"></td>
        </tr>
        <tr>
          <td>lastname</td>
          <td><input type="text" name="lastname" value="<?php echo $employee->getLastname(); ?>"></td>
        </tr>
        <tr>
          <td>commission</td>
          <td><input type="text" name="commission" value="<?php echo $employee->getCommission(); ?>"></td>
        </tr>
        <tr>
          <td>seniority</td>  
          <td><input type="text" name="seniority" value="<?php echo $employee->getSeniority(); ?>"></td>
        </tr>
        <tr>
          <td>phone number</td>
          <td><input type="text" name="phone_number" value="<?php echo $employee->getPhoneNumber(); ?>"></td>
        </tr>
        <tr>
          <td>gender</td>
          <td>
            <select name="gender">
              <option <?php if( $employee->getGender() == 'Male') { echo 'selected="selected"'; } ?>  value="Male"  >male</option>
              <option <?php if( !$employee->getGender() == 'Female') { echo 'selected="selected"'; } ?> value="Female">female</option>
            </select>     
          </td>
        </tr>
        <tr>
          <td>status</td>
          <td>
            <select name="active">
              <option <?php if( $employee->getActive() ) { echo 'selected="selected"'; } ?>  value="1"  >active</option>
              <option <?php if( !$employee->getActive() ) { echo 'selected="selected"'; } ?> value="0">inactive</option>
            </select>
          </td>
        </tr>
        <tr>
          <td>hire date</td>
          <td><input class="datepicker" type="text" name="hire_date" value="<?php echo $employee->getHireDate(); ?>"></td>
        </tr>
      </table>
      <p>Address</p>
      <table>
        <tr>
          <td>Street 1</td>
          <td><input type="text" name="street1" value="<?php echo $employee->getAddress()->getStreet1(); ?>"></td>
        </tr>
        <tr>
          <td>Street 2</td>
          <td><input type="text" name="street2" value="<?php echo $employee->getAddress()->getStreet2(); ?>"></td>
        </tr>
        <tr>
          <td>Postal Code</td>
          <td><input type="text" name="postal_code" value="<?php echo $employee->getAddress()->getPostalCode(); ?>"></td>
        </tr>
        <tr>
          <td>City</td>
          <td>
            <select id="city" class="city" name="city_id" >
                <option value="0">Select City</option>
            <?php  
              foreach ($cityList as $city) {
                echo "<option ".($city->getId() == $city_id ? 'selected="selected"' : " ")
                  ."value='".$city->getId()."'>".$city->getName()."</option>";
              }
            ?>
            </select>
          </td>
        </tr>
        <tr>
          <td>State</td>
          <td>
            <select id="state" class="state" name="state_id" >
                <option value="0">Select State</option>
            <?php  
              foreach ($stateList as $state) {
                echo "<option ".($state->getId() == $state_id ? 'selected="selected"' : " ")
                  ."value='".$state->getId()."'>".$state->getName()."</option>";
              }
            ?>
            </select>
          </td>
        </tr>
        <tr>
          <td>Country</td>
          <td>
            <select id="country" class="country" name="country_id" >
              <option value="0">Select Country</option>
            <?php  
              foreach ($countryList as $country) {
                echo "<option ".($country->getId() == $country_id ? 'selected="selected"' : " ")
                  ."value='".$country->getId()."'>".$country->getName()."</option>";
              }
            ?>
            </select>
          </td>
        </tr>
      </table>

      <div><input type="submit" name="update" value="update"></div>
    </form>

    </div>
    
    <!-- Nav Sidebar -->
    <!-- This is source ordered to be pulled to the left on larger screens -->
    <div class="large-2 pull-10 columns">
        
      <ul class="side-nav">
        <li><a href="view/sale/sale.php">Sales</a></li>
        <li><a href="view/sale/product.php">Sales By Product</a></li>
        <li><a href="activity.php">Employee Activities</a></li>
        <li><a href="information.php">Employee Info</a></li>
        <li><a href="payment.php">Employee Payment</a></li>
      </ul>
      
    </div>
    
  </div>

  
  <!-- Footer -->
  
  <footer class="row">
    <div class="large-12 columns">
      <hr>
      <div class="row">
        <div class="large-6 columns">
          <p></p>
        </div>
        <div class="large-6 columns">
          <ul class="inline-list right">
            <li><a href="#">Section 1</a></li>
            <li><a href="#">Section 2</a></li>
            <li><a href="#">Section 3</a></li>
            <li><a href="#">Section 4</a></li>
          </ul>
        </div>
      </div>
    </div> 
  </footer>
  <script>

  </script><script src="/js/zepto.js"></script>
  <script src="/js/foundation.js"></script>
  <script>
    $(document).foundation();
  </script>


</body></html>