<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperEmployee.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Login.php'); 

$login = new Login();

$mapperEmployee = new MapperEmployee();
$objects = $mapperEmployee->selectAll();


?>

<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html style="" class=" js no-touch svg inlinesvg svgclippaths no-ie8compat" lang="en"><!--<![endif]--><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width">

  <title>Employee Information</title>

  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">

  <script src="/js/custom.js"></script>

</head>
<body>

<!-- Header and Nav -->

  <div class="row">
    <div class="large-3 columns">
      <h1><img src="/images/400x100textLogo.png"></h1>
    </div>

    <div class="large-7 columns">
      <ul class="inline-list right">
      <?php if (!$login->isUserLoggedIn) { ?>
        <li><a href="/view/login/index.php">Sign In</a></li>
      <?php } else { ?>
        <li><?php echo $login->messages[0]; ?></li>
        <li><a href="/view/login/index.php?logout=1">Logout</a></li>
      <?php } ?>
      </ul>
    </div>

    <div class="large-10s pull-2 columns">
      <ul class="inline-list right">
        <li><a href="/index.php" >Home</a></li>
        <li><a href="/view/history/repair.php" >View Repair History</a></li>
        <li><a href="/view/product/index.php" >Check Inventory</a></li>
      </ul>
    </div>
  </div>

  <!-- End Header and Nav -->
  
  <div class="row">    
    
    <!-- Main Content Section -->
    <!-- This has been source ordered to come first in the markup (and on small devices) but to be to the right of the nav on larger screens -->
    <div class="large-8 push-2 columns">
      
      <h3>Employee Information <small>List</small></h3>
      <table>
        <tr>
          <td>Firstname</td>
          <td>Lastname</td>
          <td>Seniority</td>
          <td>Commission</td>
          <td>Gender</td>
          <td>Hire Date</td>
          <td>Payment History</td>
          <td></td>
        </tr>

      <?php foreach ($objects as $object) { ?> 
        <tr>
          <td><?php echo $object->getFirstname(); ?></td>    
          <td><?php echo $object->getLastname(); ?></td>    
          <td><?php echo $object->getSeniority(); ?></td>    
          <td><?php echo $object->getCommission(); ?></td>    
          <td><?php echo $object->getGender(); ?></td>    
          <td><?php echo $object->getHireDate(); ?></td> 
          <td><a href="payment.php?id=<?php echo $object->getId(); ?>">View</a></td> 
          <td><a href="edit.php?id=<?php echo $object->getId(); ?>">Edit</a></td> 
        </tr>
      <?php } ?>
      </table>

    </div>
    

<div class="large-2 push-2 columns">
      <h3></h3>
       <p></p>
</div>
    
    <!-- Nav Sidebar -->
    <!-- This is source ordered to be pulled to the left on larger screens -->
    <div class="large-2 pull-10 columns">
        
      <ul class="side-nav">
        <li><a href="/view/sale/sale.php">Sales</a></li>
        <li><a href="/view/sale/product.php">Sales By Product</a></li>
        <li><a href="activity.php">Employee Activities</a></li>
        <li><a href="information.php">Employee Info</a></li>
        <li><a href="payment.php">Employee Payment</a></li>
      </ul>
        
    </div>
    
  </div>
    
  
  <!-- Footer -->
  
  <footer class="row">
    <div class="large-12 columns">
      <hr>
      <div class="row">
        <div class="large-6 columns">
          <p></p>
        </div>
        <div class="large-6 columns">
          <ul class="inline-list right">
            <li><a href="#">Section 1</a></li>
            <li><a href="#">Section 2</a></li>
            <li><a href="#">Section 3</a></li>
            <li><a href="#">Section 4</a></li>
          </ul>
        </div>
      </div>
    </div> 
  </footer>
  
  <script src="/js/zepto.js"></script>
  <script src="/js/foundation.js"></script>
  <script>
    $(document).foundation();
  </script>

</body></html>