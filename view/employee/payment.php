<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperEmployee.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Employee.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Login.php'); 

$login = new Login();

$mapperEmployee = new MapperEmployee();

$employeeList = $mapperEmployee->selectAll(); 

if( isset($_GET['id']) 
    && is_numeric($_GET['id']) 
    && $_GET['id'] > 0 
  ) {
  $employee_id = $_GET['id'];
} else if (isset($_POST['filter_employee_id'])) {
  $employee_id = $_POST['filter_employee_id'];
} else {
  $employee_id  = 0;  
}

if ($employee_id > 0) {
  $employee = $mapperEmployee->findById($employee_id);
} else {
  $employee = new Employee();
}

if (isset($_POST['filter_frequency'])) 
{
  $frequency = $_POST['filter_frequency'];
} else {
  $frequency  = 'Week';  
}

$objects = $mapperEmployee->getPaymentHistoryWhereFrequencyEmployeeOrder($frequency, $employee);

function getStartAndEndDate($week, $year)
{

    $time = strtotime("1 January $year", time());
    $day = date('w', $time);
    $time += ((7*$week)+1-$day)*24*3600;
    $return[0] = date('Y-n-j', $time);
    $time += 6*24*3600;
    $return[1] = date('Y-n-j', $time);
    return $return;
}

?>

<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html style="" class=" js no-touch svg inlinesvg svgclippaths no-ie8compat" lang="en"><!--<![endif]--><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width">

  <title>Employee Payment History</title>

  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">

  <script src="/js/custom.js"></script>

</head>
<body>

<!-- Header and Nav -->

  <div class="row">
    <div class="large-3 columns">
      <h1><img src="/images/400x100textLogo.png"></h1>
    </div>

    <div class="large-7 columns">
      <ul class="inline-list right">
      <?php if (!$login->isUserLoggedIn) { ?>
        <li><a href="/view/login/index.php">Sign In</a></li>
      <?php } else { ?>
        <li><?php echo $login->messages[0]; ?></li>
        <li><a href="/view/login/index.php?logout=1">Logout</a></li>
      <?php } ?>
      </ul>
    </div>

    <div class="large-10s pull-2 columns">
      <ul class="inline-list right">
        <li><a href="/index.php">Home</a></li>
        <li><a href="/view/history/repair.php">View Repair History</a></li>
        <li><a href="/view/product/index.php">Check Inventory</a></li>
      </ul>
    </div>
  </div>

  <!-- End Header and Nav -->
  
  <div class="row">    
    
    <!-- Main Content Section -->
    <!-- This has been source ordered to come first in the markup (and on small devices) but to be to the right of the nav on larger screens -->
    <div class="large-6 push-2 columns">
      
      <h3>Employee Payment History 
        <small>
          <?php if ($employee instanceof Employee && $employee->getId() > 0) { 
            $employee->getFirstname().' '.$employee->getLastname(); 
          } ?>
        </small></h3>
      <table>
        <tr>
          <td>Date (<?php echo $frequency; ?>)</td>
        <?php if ($frequency == 'Week') { ?>
          <td>End Date (<?php echo $frequency;?>)</td>
        <?php } ?>  
          <td>Employee Name</td>
          <td>ID</td>
          <td>Commission</td>
        </tr>

      <?php foreach ($objects as $object) { ?> 
        <tr>
        <?php if ($frequency == 'Week') { ?>
          <td><?php echo getStartAndEndDate($object->frequency, $object->year)[0]; ?></td>
          <td><?php echo getStartAndEndDate($object->frequency, $object->year)[1]; ?></td>
        <?php } else if($frequency == 'Month') { ?>
          <td><?php echo $object->year.' '.date("F", mktime(0, 0, 0, $object->frequency, 10));; ?></td>
        <?php } else if($frequency == 'Year') { ?>
          <td><?php echo $object->frequency; ?></td>
        <?php } ?>
        <td><?php echo $object->employee_name; ?></td> 
          <td><?php echo $object->employee_id; ?></td> 
          <td><?php echo substr($object->commission, 0, strpos($object->commission, '.')+3);; ?></td> 
        </tr>
      <?php } ?>
      </table>

    </div>
    

<div class="large-4 push-2 columns">
      <h3>Filters</h3>
       <div> 
          <form action="payment.php" method="post" >
          <div>
            Display History by
            <select class="large-3" name="filter_frequency">
              <option value="Week" <?php if ($frequency == 'Week') {echo 'selected="selected"';}  ?> >Week</option>
              <option value="Month" <?php if ($frequency == 'Month') {echo 'selected="selected"';}  ?> >Month</option>
              <option value="Year" <?php if ($frequency == 'Year') {echo 'selected="selected"';} ?> >Year</option>
            </select>
          </div>
          <div>
            Select Employee
            <select class="large-6" name="filter_employee_id">
              <option value="0">All</option>
            <?php  
              foreach ($employeeList as $employeeObject) {
                echo "<option ".($employeeObject->getId() == $employee_id ? 'selected="selected"' : " ")
                  ."value='".$employeeObject->getId()."'>"
                  .$employeeObject->getFirstname().' '.$employeeObject->getLastname()."</option>";
              }
            ?>
            </select>
          </div>
          <input type="submit" name="submit" value="filter">
          </form>
       </div>
</div>
    
    <!-- Nav Sidebar -->
    <!-- This is source ordered to be pulled to the left on larger screens -->
    <div class="large-2 pull-10 columns">
        
      <ul class="side-nav">
        <li><a href="/view/sale/sale.php">Sales</a></li>
        <li><a href="/view/sale/product.php">Sales By Product</a></li>
        <li><a href="activity.php">Employee Activities</a></li>
        <li><a href="information.php">Employee Info</a></li>
        <li><a href="payment.php">Employee Payment</a></li>
      </ul>
        
    </div>
    
  </div>
    
  
  <!-- Footer -->
  
  <footer class="row">
    <div class="large-12 columns">
      <hr>
      <div class="row">
        <div class="large-6 columns">
          <p></p>
        </div>
        <div class="large-6 columns">
          <ul class="inline-list right">
            <li><a href="#">Section 1</a></li>
            <li><a href="#">Section 2</a></li>
            <li><a href="#">Section 3</a></li>
            <li><a href="#">Section 4</a></li>
          </ul>
        </div>
      </div>
    </div> 
  </footer>
  <script>

  </script><script src="/js/zepto.js"></script>
  <script src="/js/foundation.js"></script>
  <script>
    $(document).foundation();
  </script>


</body></html>