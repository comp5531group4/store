<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperPurchase.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperEmployee.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/FormValidator.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Login.php'); 

$login = new Login();

$mapperEmployee = new MapperEmployee();
$employeeList = $mapperEmployee->selectAll();

$mapperPurchase = new MapperPurchase();

$employee_id = 0;
$employee = array();
$totalEmployeeCommission = 0;

$filter_date_from = date("Y")."-01-01";
$filter_date_to   = date("Y-m-d");

// get id from query string on page load
if ( isset($_GET['id']) )
{
  $employee_id = $_GET['id'];
  if ($employee_id && is_numeric($employee_id) && $employee_id > 0) {
    $employee = $mapperEmployee->findById($employee_id);
  } else {
    $employee_id = 0;
    $employee = array();
  }
} 

// process form
if ( isset($_POST['employee_id']) )
{
  $employee_id      = $_POST['employee_id'];
  if (is_numeric($employee_id) && $employee_id > 0) {
    $employee = $mapperEmployee->findById($employee_id);
  } else {
    $employee = new Employee();
  }

  $filter_date_from = $_POST['from_date'];
  $filter_date_to   = $_POST['to_date']; 
  
  $isFromDateSmaller = true;
  if ($filter_date_from > $filter_date_to) 
  {
    echo "<div>Error. 'From Date' cannot be greater than 'To Date'</div>";
    $isFromDateSmaller = false;
  }

  // form validation
  $validations = array(
      'from_date'=>'date',
      'to_date'=>'date'
  );
  $required = array('from_date', 'to_date');
  $sanatize = array();
  $validator = new FormValidator($validations, $required, $sanatize);
  if( $isFromDateSmaller && $validator->validate($_POST) )
  {
    $objects = $mapperPurchase->selectAllPurchaseServiceWhereEmployeeIDDate($employee_id, $filter_date_from, $filter_date_to);
  } 
} 
else 
{
  $objects = $mapperPurchase->selectAllPurchaseService();  
}

?>

<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html style="" class=" js no-touch svg inlinesvg svgclippaths no-ie8compat" lang="en"><!--<![endif]--><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width">

  <title>Employee Activities</title>

  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet">

  <script src="/js/custom.js"></script>

  <script src="/js/jquery.min.js"></script>
  <script src="/js/jquery-ui-1.10.3.custom.min.js"></script>

  <script>
    $(function() {
      $( ".datepicker" ).datepicker( { dateFormat: "yy-mm-dd" } );
    }); 
  </script>

</head>
<body>


<!-- Header and Nav -->

  <div class="row">
    <div class="large-3 columns">
      <h1><img src="/images/400x100textLogo.png"></h1>
    </div>

    <div class="large-7 columns">
      <ul class="inline-list right">
      <?php if (!$login->isUserLoggedIn) { ?>
        <li><a href="/view/login/index.php">Sign In</a></li>
      <?php } else { ?>
        <li><?php echo $login->messages[0]; ?></li>
        <li><a href="/view/login/index.php?logout=1">Logout</a></li>
      <?php } ?>
      </ul>
    </div>

    <div class="large-10s pull-2 columns">
      <ul class="inline-list right">
        <li><a href="/index.php">Home</a></li>
        <li><a href="/view/history/repair.php">View Repair History</a></li>
        <li><a href="/view/product/index.php">Check Inventory</a></li>
      </ul>
    </div>
  </div>

  <!-- End Header and Nav -->
  
  <div class="row"> 
    
    <div class="large-10 push-6 columns">
          <form id="search_history" name="filter_date" method="post" action="activity.php">
            <p class="large-4 push-2"><b>Filters</b></p>
            <select class="large-4 push-2" name="employee_id">
              <option value="0">Select Employee</option>
            <?php  
              foreach ($employeeList as $employeeObject) {
                echo "<option ".($employeeObject->getId() == $employee_id ? 'selected="selected"' : " ")
                  ."value='".$employeeObject->getId()."'>"
                  .$employeeObject->getFirstname().' '.$employeeObject->getLastname()."</option>";
              }
            ?>
            </select>
            
            <ul class="push-2 inline-list">
              <li>From Date<input type="text" class="datepicker" name="from_date" value="<?php echo $filter_date_from; ?>" /></li>
              <li>To Date<input type="text" class="datepicker" name="to_date" value="<?php echo $filter_date_to; ?>" /></li>
            </ul>
            <button type="submit" class="push-2 button" />search</button>
          </form>
    </div>

    <!-- Nav Sidebar -->
    <!-- This is source ordered to be pulled to the left on larger screens -->
    <div class="large-2 pull-10 columns">
        
      <ul class="side-nav">
        <li><a href="/view/sale/sale.php">Sales</a></li>
        <li><a href="/view/sale/product.php">Sales By Product</a></li>
        <li><a href="/view/employee/activity.php">Employee Activities</a></li>
        <li><a href="/view/employee/information.php">Employee Info</a></li>
        <li><a href="/view/employee/payment.php">Employee Payment</a></li>
      </ul>
        
    </div>

  </div>

  <div class="row">    
    
    <!-- Main Content Section -->
    <!-- This has been source ordered to come first in the markup (and on small devices) but to be to the right of the nav on larger screens -->
    <div class="large-10 push-2 columns">
      
      <h3>Employee Activities 
        <small>
          <?php if($employee_id > 0) { echo $employee->getFirstname()." ".$employee->getLastname(); } ?>
        </small>
      </h3>
      <table>
        <tr>
          <td>Date</td>
          <td>Employee</td>
          <td>Service</td>
          <td>Unit Price</td>
          <td>Quantity</td>
          <td>Discount</td>
          <td>Commission</td>
          <td>Store Revenu</td>
        </tr>

      <?php foreach ($objects as $object) { 
            if($employee_id > 0) { $totalEmployeeCommission += $object->commission; }
        ?> 
        <tr>
          <td><?php echo $object->date; ?></td>    
          <td><?php echo $object->employee_name; ?></td>    
          <td><?php echo $object->service_name; ?></td>    
          <td><?php echo $object->unit_price; ?></td>    
          <td><?php echo $object->quantity; ?></td>    
          <td><?php echo $object->discount; ?></td>    
          <td><?php echo substr($object->commission, 0, strpos($object->commission, '.')+3);; ?></td> 
          <td><?php echo substr($object->revenue, 0, strpos($object->revenue, '.')+3);; ?></td> 
        </tr>
      <?php } ?>
      </table>

      <?php if($employee_id > 0) { ?>
      <div>
        <p>
          Total employee commission between 
            <?php echo $filter_date_from." and ".$filter_date_to.": $".$totalEmployeeCommission;?>
        </p>
      </div>
      <?php } ?>

    </div>
        
  </div>
    
  
  <!-- Footer -->
  
  <footer class="row">
    <div class="large-12 columns">
      <hr>
      <div class="row">
        <div class="large-6 columns">
          <p></p>
        </div>
        <div class="large-6 columns">
          <ul class="inline-list right">
            <li><a href="#">Section 1</a></li>
            <li><a href="#">Section 2</a></li>
            <li><a href="#">Section 3</a></li>
            <li><a href="#">Section 4</a></li>
          </ul>
        </div>
      </div>
    </div> 
  </footer>
  <script>

  </script><script src="/js/zepto.js"></script>
  <script src="/js/foundation.js"></script>
  <script>
    $(document).foundation();
  </script>


</body></html>