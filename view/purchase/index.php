<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperPurchase.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Login.php'); 

$login = new Login();

$mapperPurchase = new MapperPurchase();

$objects = array();

if( isset($_POST['filter_purchase_id'])
      && is_numeric( $_POST['filter_purchase_id'] ) 
      && $_POST['filter_purchase_id'] > 0 ) {
  $filter_purchase_id = $_POST['filter_purchase_id'];
} else {
  $filter_purchase_id = 0;
}

if ($filter_purchase_id > 0) {
  $objectsService = $mapperPurchase->selectAllPurchaseService($filter_purchase_id);
  $objectsProduct = $mapperPurchase->selectAllPurchaseProduct($filter_purchase_id);

  print_r($objectsService);
  print_r($objectsProduct);

} else {
  $objectsService = array();
  $objectsProduct = array();
}

?>

<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html style="" class=" js no-touch svg inlinesvg svgclippaths no-ie8compat" lang="en"><!--<![endif]--><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width">

  <title>Inventory</title>

  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">

  <script src="/js/custom.js"></script>

</head>
<body>


<!-- Header and Nav -->

  <div class="row">
    <div class="large-3 columns">
      <h1><img src="/images/400x100textLogo.png"></h1>
    </div>

    <div class="large-7 columns">
      <ul class="inline-list right">
      <?php if (!$login->isUserLoggedIn) { ?>
        <li><a href="/view/login/index.php">Sign In</a></li>
      <?php } else { ?>
        <li><?php echo $login->messages[0]; ?></li>
        <li><a href="/view/login/index.php?logout=1">Logout</a></li>
      <?php } ?>
      </ul>
    </div>

    <div class="large-10s pull-2 columns">
      <ul class="inline-list right">
        <li><a href="/index.html">Home</a></li>
        <li><a href="/view/history/repair.php">View Repair History</a></li>
        <li><a href="/view/product/index.php">Check Inventory</a></li>
      </ul>
    </div>
  </div>

  <!-- End Header and Nav -->
  
  <div class="row">    
    
    <!-- Main Content Section -->
    <!-- This has been source ordered to come first in the markup (and on small devices) but to be to the right of the nav on larger screens -->
    <div class="large-6 push-2 columns">
      
      <h3>Purchase <small>Invoice</small></h3>
      <table>
        <tr>
          <td></td>
        </tr>

      <?php foreach ($objects as $object) { ?> 
        <tr>
          <td></td>        
        </tr>
      <?php } ?>
      </table>
    </div>
    
<div class="large-4 push-2 columns">
      <h3>Filters</h3>
       <div>
          <form action="index.php" method="post" >
          Search by Purchase ID
          <input type="text" name="filter_purchase_id" value="<?php echo $filter_purchase_id; ?>"/>
          <input type="submit" name="submit" value="filter">
          </form>
      </div>
</div>
    
    <!-- Nav Sidebar -->
    <!-- This is source ordered to be pulled to the left on larger screens -->
    <div class="large-2 pull-10 columns">
        
      <ul class="side-nav">
        <li><a href="/view/sale/sale.php">Sales</a></li>
        <li><a href="/view/sale/product.php">Sales By Product</a></li>
        <li><a href="/view/employee/activity.php">Employee Activities</a></li>
        <li><a href="/view/employee/information.php">Employee Info</a></li>
        <li><a href="/view/employee/payment.php">Employee Payment</a></li>
      </ul>
              
    </div>
    
  </div>
    
  
  <!-- Footer -->
  
  <footer class="row">
    <div class="large-12 columns">
      <hr>
      <div class="row">
        <div class="large-6 columns">
          <p></p>
        </div>
        <div class="large-6 columns">
          <ul class="inline-list right">
            <li><a href="#">Section 1</a></li>
            <li><a href="#">Section 2</a></li>
            <li><a href="#">Section 3</a></li>
            <li><a href="#">Section 4</a></li>
          </ul>
        </div>
      </div>
    </div> 
  </footer>
  <script>

  </script><script src="/js/zepto.js"></script>
  <script src="/js/foundation.js"></script>
  <script>
    $(document).foundation();
  </script>


</body></html>