<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperCountry.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Country.php');

if ($_GET['country_id'])
{
	$country_id = $_GET['country_id'];

	if (is_numeric($country_id) && $country_id > 0)
	{
		$mapperCountry = new MapperCountry();
		$statesList = $mapperCountry->selectAllStatesWhereCountry($country_id);

		echo json_encode($statesList);
	}
}

?>