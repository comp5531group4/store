<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperState.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/State.php');

if ($_GET['state_id'])
{
	$state_id = $_GET['state_id'];

	if (is_numeric($state_id) && $state_id > 0)
	{
		$mapperState = new MapperState();
		$cityList = $mapperState->selectAllCitiesWhereState($state_id);

		echo json_encode($cityList);
	}
}

?>