<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperProduct.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperCategoryProduct.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Login.php'); 

$login = new Login();

$mapperProduct = new MapperProduct();

$mapperCategoryProduct = new MapperCategoryProduct();
$categoryProductList = $mapperCategoryProduct->selectAll(); 

$where = array();

if( isset($_GET['id']) 
    && is_numeric($_GET['id']) 
    && $_GET['id'] > 0 
  ) {
  $category_product_id = $_GET['id'];
} else if (isset($_POST['filter_category_product_id'])) {
  $category_product_id = $_POST['filter_category_product_id'];
} else {
  $category_product_id  = 0;  
}

if (isset($_POST['filter_product']) ) 
{
  $filter_product = $_POST['filter_product'];
  
  if ($filter_product == "") 
  {
    // do nothing
  }
  else if ( is_numeric($filter_product) ) 
  {
    $where[] = array(
              'row_name'=>'product_id',
              'operator'=>' = ', 
              'value'=>$filter_product,
              'value_type'=>'integer'
        );
  }
  else 
  {
    $where[] = array(
              'row_name'=>'name',
              'operator'=>' LIKE ', 
              'value'=>"%".$filter_product."%",
              'value_type'=>'string'
        );  
  }
} 
else 
{
  $filter_product = "";
}

if ($category_product_id > 0) {
  $where[] = array(
              'row_name'=>'category_product_id',
              'operator'=>' = ', 
              'value'=>$category_product_id,
              'value_type'=>'integer'
        );  
} 

if ($where) {
  $objects = $mapperProduct->selectWhere($where);  
} else {
  $objects = $mapperProduct->selectAll();  
}

?>

<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html style="" class=" js no-touch svg inlinesvg svgclippaths no-ie8compat" lang="en"><!--<![endif]--><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width">

  <title>Inventory</title>

  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">

  <script src="/js/custom.js"></script>

</head>
<body>


<!-- Header and Nav -->

  <div class="row">
    <div class="large-3 columns">
      <h1><img src="/images/400x100textLogo.png"></h1>
    </div>

    <div class="large-7 columns">
      <ul class="inline-list right">
      <?php if (!$login->isUserLoggedIn) { ?>
        <li><a href="/view/login/index.php">Sign In</a></li>
      <?php } else { ?>
        <li><?php echo $login->messages[0]; ?></li>
        <li><a href="/view/login/index.php?logout=1">Logout</a></li>
      <?php } ?>
      </ul>
    </div>

    <div class="large-10s pull-2 columns">
      <ul class="inline-list right">
        <li><a href="/index.php">Home</a></li>
        <li><a href="/view/history/repair.php">View Repair History</a></li>
        <li><a href="/view/product/index.php">Check Inventory</a></li>
      </ul>
    </div>
  </div>

  <!-- End Header and Nav -->
  
  <div class="row">    
    
    <!-- Main Content Section -->
    <!-- This has been source ordered to come first in the markup (and on small devices) but to be to the right of the nav on larger screens -->
    <div class="large-6 push-2 columns">
      
      <h3>Inventory <small>Store Catalog</small></h3>
      <table>
        <tr>
          <td>Product ID</td>
          <td>Name</td>
          <td>Category</td>
          <td>Units In Stock</td>
          <td>Units On Order</td>
          <td>Price</td>
        </tr>

      <?php foreach ($objects as $object) { ?> 
        <tr>
          <td><?php echo $object->getId(); ?></td>    
          <td><?php echo $object->getName(); ?></td>  
          <td><?php echo $object->getCategory()->getDescription(); ?></td>    
          <td><?php echo $object->getUnitsInStock(); ?></td>    
          <td><?php echo $object->getUnitsOnOrder(); ?></td> 
          <td><?php echo $object->getPrice(); ?></td>     
        </tr>
      <?php } ?>
      </table>
    </div>
    
<div class="large-4 push-2 columns">
      <h3>Filters</h3>
       <div>
          <form action="index.php" method="post" >
            Filter by Category
            <br/>
            <select class="large-6" name="filter_category_product_id">
              <option value="0">All</option>
            <?php  
              foreach ($categoryProductList as $categoryProductObject) {
                echo "<option ".($categoryProductObject->getId() == $category_product_id ? 'selected="selected"' : " ")
                  ."value='".$categoryProductObject->getId()."'>"
                  .$categoryProductObject->getDescription()."</option>";
              }
            ?>
            </select>
            <br/>
          Search by Product Name or ID
          <input type="text" name="filter_product" value="<?php echo $filter_product; ?>"/>
          <input type="submit" name="submit" value="filter">
          </form>
      </div>
</div>
    
    <!-- Nav Sidebar -->
    <!-- This is source ordered to be pulled to the left on larger screens -->
    <div class="large-2 pull-10 columns">
        
      <ul class="side-nav">
        <?php if ($login->getRoleId() == 1) { ?>
        <li><a href="/view/sale/sale.php">Sales</a></li>
        <li><a href="/view/sale/product.php">Sales By Product</a></li>
        <li><a href="/view/employee/activity.php">Employee Activities</a></li>
        <li><a href="/view/employee/information.php">Employee Info</a></li>
        <li><a href="/view/employee/payment.php">Employee Payment</a></li>
        <?php } ?>
      </ul>
              
    </div>
    
  </div>
    
  
  <!-- Footer -->
  
  <footer class="row">
    <div class="large-12 columns">
      <hr>
      <div class="row">
        <div class="large-6 columns">
          <p></p>
        </div>
        <div class="large-6 columns">
          <ul class="inline-list right">
            <li><a href="#">Section 1</a></li>
            <li><a href="#">Section 2</a></li>
            <li><a href="#">Section 3</a></li>
            <li><a href="#">Section 4</a></li>
          </ul>
        </div>
      </div>
    </div> 
  </footer>
  <script>

  </script><script src="/js/zepto.js"></script>
  <script src="/js/foundation.js"></script>
  <script>
    $(document).foundation();
  </script>


</body></html>