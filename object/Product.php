<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/object/AbstractObject.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperCategoryProduct.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/CategoryProduct.php');

class Product extends AbstractObject
{
	protected $category_product_id; 	
 	protected $name;
 	protected $description;
 	protected $units_in_stock;
 	protected $units_on_order;
 	protected $price;
 	protected $picture;

	public function __construct()
	{
		parent::__construct();
		// type: foreign key
		$this->category_product_id 	= 0;

		// type: string
 		$this->name 				= "";
 		$this->description 			= "";
 		$this->picture 				= "";

 		// type: int 
 		$this->units_in_stock 		= 0;
 		$this->units_on_order 		= 0;
 		$this->price 				= 0;
	}
	
	public function getCategory()
	{
		if ($this->category_product_id > 0)
		{
			$mapperCategory = new MapperCategoryProduct();
			return $mapperCategory->findById($this->category_product_id);
		} else {
			return new CategoryProduct();
		}
	}

	public function getCategoryId()
	{
		return $this->category_product_id;
	}

	public function setCategoryId($category_product_id)
	{
		if (is_numeric($category_product_id) && $category_product_id > 0) {
			$this->category_product_id = $category_product_id;
		} else {
			$this->category_product_id = 0;
		}
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getPicture()
	{
		return $this->picture;
	}

	public function setPicture($picture)
	{
		$this->picture = $picture;
	}

	public function getUnitsInStock()
	{
		return $this->units_in_stock;
	}

	public function setUnitsInStock($units_in_stock)
	{
		$this->units_in_stock = $units_in_stock;
	}

	public function getUnitsOnOrder()
	{
		return $this->units_on_order;
	}

	public function setUnitsOnOrder($units_on_order)
	{
		$this->units_on_order = $units_on_order;
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function setPrice($price)
	{
		$this->price = $price;
	}

	public function toAssociativeArray()
	{	
		$product = array(
			array('category_product_id', $this->category_product_id, "integer"), 
	 		array('name', $this->name, "string"),
	 		array('description', $this->description, "string"),
	 		array('picture', $this->picture , "string"),
	 		array('units_in_stock', $this->units_in_stock, "integer"),
	 		array('units_on_order', $this->units_on_order, "integer"),
	 		array('price', $this->price, "integer"),
	 	);

		if ( !($this->getId() == 0) ) {
			$product[] = array('product_id',  $this->getId(),   "integer");
		}

		return $product;
	}
}
?>
