<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/object/AbstractObject.php');

class Customer extends AbstractObject
{ 	
 	protected $address_id;
 	protected $firstname;
 	protected $lastname; 
 	protected $email; 	
 	protected $phone_number;
 	protected $create_date;

	public function __construct()
	{
		parent::__construct();
		$this->address_id   = 0;

		// type: string
		$this->firstname 	= "";
		$this->lastname 	= "";
		$this->phone_number = "";
		$this->email 	 	= "";
		
		// type: date
		$this->create_date  = date("Y-m-d");
	}
	
	public function getAddress()
	{
		return $this->address_id;
	}

	public function setAddressId($address_id)
	{
		if (is_numeric($address_id) && $address_id > 0) {
			$this->address_id = $address_id;
		} else {
			$this->address_id = new Address();
		}
	}

	public function getFirstname()
	{
		return $this->firstname;
	}

	public function setFirstname($fname)
	{
		$this->firstname = $fname;
	}

	public function getLastname()
	{
		return $this->lastname;
	}

	public function setLastname($lname)
	{
		$this->lastname = $lname;
	}

	public function getPhoneNumber()
	{
		return $this->phone_number;
	}

	public function setPhoneNumber($phone_number)
	{
		$this->phone_number = $phone_number;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setEmail($email)
	{
		$this->email = $email;
	}

	public function getCreateDate()
	{
		return $this->create_date;
	}

	public function setCreateDate($create_date)
	{
		$this->create_date = $create_date;
	}

	public function toAssociativeArray()
	{	
		$customer = array(
	 		array('address_id',  $this->address_id,  "integer"),
			array('firstname', $this->firstname, "string"),
			array('lastname',  $this->lastname,  "string"),
			array('phone_number',  $this->phone_number,  "string"),
			array('email',  $this->email,  "string"),
	 		array('create_date',  $this->create_date,  "string")
	 	);

		if ( !($this->getId() == 0) ) {
			$customer[] = array('customer_id',  $this->getId(),   "integer");
		}

		return $customer;
	}
}
?>
