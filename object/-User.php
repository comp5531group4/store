<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/object/AbstractObject.php');

class User extends AbstractObject
{
 	protected $role_id;
 	protected $username;
 	protected $password;	

	public function __construct()
	{
		parent::__construct();
		// type: foreign key (default 1)
		$this->user_id = 1; 	

		// type: string
 		$this->username 		= "";
 		$this->password 		= "";
	}
	
	public function getRoleId()
	{
		return $this->role_id;
	}

	public function setRoleId($role_id)
	{
		$this->role_id = $role_id;
	}

	public function getUsername()
	{
		return $this->username;
	}

	public function setUsername($username)
	{
		$this->username = $username;
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function setPassword($password)
	{
		$this->password = $password;
	}

	public function toAssociativeArray()
	{	
		$user = array(
			array('role_id', $this->$role_id, "integer"), 
	 		array('username', $this->$username, "string"),
	 		array('password', $this->$password, "string")
	 	);

		if ( !($this->getId() == 0) ) {
			$user[] = array('user_id',  $this->getId(),   "integer");
		}

		return $user;
	}
}
?>
