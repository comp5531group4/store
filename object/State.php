<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/object/AbstractObject.php');

include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperCountry.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Country.php');

class State extends AbstractObject
{
 	protected $country_id;
	protected $name;
	
	public function __construct()
	{
		parent::__construct();

		$this->country_id 	= 0;
		$this->name			= "";
	}
	
	public function getCountry()
	{
		if ($this->country_id > 0)
		{
			$mapperCountry = new MapperCountry();
			return $mapperCountry->findById($this->country_id);
		} else {
			return new Country();
		}
	}

	public function getCountryId()
	{
		return $this->country_id;
	}

	public function setCountryId( $country_id )
	{
		if ( is_numeric($country_id) && $country_id > 0 ) {
			$this->country_id = $country_id;
		} else {
			$this->country_id = 0;
		}
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;	
	}

	public function toAssociativeArray()
	{	
		$state = array(
			array('country_id', $this->country_id, "integer"),
			array('name', $this->name, "string"),
	 	);	

		if ( !($this->getId() == 0) ) {
			$state[] = array('state_id',  $this->getId(),   "integer");
		}

		return $state;
	}
}
?>
