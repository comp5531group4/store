<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/object/AbstractObject.php');

class Country extends AbstractObject
{
	protected $name;
	
	public function __construct()
	{
		$this->name		= "";
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;	
	}

	public function toAssociativeArray()
	{	
		$country = array(
			array('name', $this->name, "string"),
	 	);	

		if ( !($this->getId() == 0) ) {
			$country[] = array('country_id',  $this->getId(),   "integer");
		}

		return $country;
	}
}
?>
