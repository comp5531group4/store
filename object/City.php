<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/object/AbstractObject.php');

include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperState.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/State.php');

class City extends AbstractObject
{
 	protected $state_id;
	protected $name;
	
	public function __construct()
	{
		parent::__construct();

		$this->state_id	= new State();
		$this->name		= "";
	}
	
	public function getState()
	{
		if ($this->state_id > 0)
		{
			$mapperState = new MapperState();
			return $mapperState->findById($this->state_id);
		} else {
			return new State();
		}
	}

	public function getStateId()
	{
		return $this->state_id;
	}

	public function setStateId( $state_id )
	{
		if ( is_numeric($state_id) && $state_id > 0 ) {
			$this->state_id = $state_id;	
		} else {
			$this->state_id = 0;
		}
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;	
	}

	public function toAssociativeArray()
	{	
		$city = array(
			array('state_id', $this->state_id, "integer"),
			array('name', $this->name, "string"),
	 	);	

		if ( !($this->getId() == 0) ) {
			$city[] = array('city_id',  $this->getId(),   "integer");
		}

		return $city;
	}
}
?>
