<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/object/AbstractObject.php');

include_once($_SERVER['DOCUMENT_ROOT'].'/object/City.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperCity.php');

class Address extends AbstractObject
{
 	protected $city_id;
	protected $street1;
	protected $street2;
	protected $postal_code;
	
	public function __construct()
	{
		parent::__construct();

		$this->city_id		= 0;
		$this->street1		= "";
		$this->street2		= "";
		$this->postal_code	= "";
	}
	
	public function getCity()
	{
		if ($this->city_id > 0)
		{
			$mapperCity = new MapperCity();
			return $mapperCity->findById($this->city_id);
		} else {
			return new City();
		}
	}

	public function getCityId()
	{
		return $this->city_id;
	}

	public function setCityId( $city_id )
	{
		if ( is_numeric($city_id) && $city_id > 0 ) {
			$this->city_id = $city_id;	
		} else {
			$this->city_id = 0;
		}
	}

	public function getStreet1()
	{
		return $this->street1;
	}

	public function setStreet1($street1)
	{
		$this->street1 = $street1;	
	}

	public function getStreet2()
	{
		return $this->street2;
	}

	public function setStreet2($street2)
	{
		$this->street2 = $street2;	
	}

	public function getPostalCode()
	{
		return $this->postal_code;
	}

	public function setPostalCode($postal_code)
	{
		$this->postal_code = $postal_code;	
	}

	public function toAssociativeArray()
	{	
		$address = array(
			array('city_id', $this->city_id, "integer"),
			array('street1', $this->street1, "string"),
			array('street2', $this->street2, "string"),
			array('postal_code', $this->postal_code, "string")
	 	);	

		if ( !($this->getId() == 0) ) {
			$address[] = array('address_id',  $this->getId(),   "integer");
		}

		return $address;
	}
}
?>
