<?php 
abstract class AbstractObject
{
    protected $id;

    public function __construct()
	{
		$this->setId(0);
	}

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    abstract public function toAssociativeArray();
}
?>
