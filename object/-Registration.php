<?php

include_once('./includes/mysql.php');

class Registration 
{
    private     $db_connection;
    
    private     $username;
    private     $password;
    private     $role_id;
    
    public      $registration_successful;

    public      $errors;
    public      $messages;
    
    public function __construct() 
    {    
        // creating a database connection
        $this->db_connection = new MySql();
        $this->db_connection->dbConnect();               
    
        $this->username                 = "";                       
        $this->password                 = "";    
        $this->role_id                  = 1;
    
        $this->registration_successful  = false;

        $this->errors                   = array();                  
        $this->messages                 = array();                  

        if (isset($_POST["register"])) 
        {            
            $this->registerNewUser();
        }        
    }

    private function registerNewUser() 
    {    
        $isValidFields = TRUE;
        // verify username field
        if ( empty($_POST['username']) ) {  
            $this->errors[] = "Username field is empty.";
            $isValidFields = FALSE;
        }

        // verify password field
        if ( empty($_POST['password']) || empty( $_POST['password_repeat']) ) {
            $this->errors[] = "Password field is empty.";
            $isValidFields = FALSE;            
        }

        // verify passwords match
        if ( $_POST['password'] !== $_POST['password_repeat']) {
            $this->errors[] = "Passwords do not match.";   
            $isValidFields = FALSE;
        }

        // verify password length
        if (strlen($_POST['password']) < 6) {
            $this->errors[] = "Password must have a minimum length of at least 6 characters"; 
            $isValidFields = FALSE;           
        }    

        // verify username length
        if (strlen($_POST['username']) > 64 || strlen($_POST['username']) < 2) {
            $this->errors[] = "Username cannot be shorter than 2 or longer than 64 characters";
            $isValidFields = FALSE;
        }
    
        // verufy username allowed characters
        if (!preg_match('/^[a-z_\-0-9]{2,64}$/i', $_POST['username']))  {    
            $this->errors[] = "Username does not fit the name sheme: only a-Z, underscores and numbers are allowed, 2 to 64 characters";
            $isValidFields = FALSE;
        } 

        /*
        // verify email
        if (empty($_POST['user_email'])) {    
            $this->errors[] = "Email field cannot be empty";
        } elseif (strlen($_POST['user_email']) > 64) {
            $this->errors[] = "Email cannot be longer than 64 characters";
        } elseif (!filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = "Your email adress is not in a valid email format";
        }
        */

        if ($isValidFields) 
        {
            $this->username = $_POST['username'];    
            $this->password = $_POST['password'];

            // check if user already exists
            $query = "SELECT * FROM user WHERE username='$this->username';";
            $isAlreadyRegistered = $this->db_connection->selectFreeRun($query);            
            $query = NULL;

            if ($isAlreadyRegistered->num_rows == 1) 
            {
                $this->errors[] = "Sorry, that user name is already taken.<br/>Please choose another one.";
            } 
            else 
            {
                $query = "INSERT INTO user (role_id, username, password) VALUES($this->role_id, '".$this->username."', '".$this->password."');";
                $insertSqlResult = $this->db_connection->selectFreeRun($query);

                if ($insertSqlResult) {
                    $this->messages[] = "Your account has been created successfully. You can now log in.";
                    $this->registration_successful = true;
                } else {
                    $this->errors[] = "Sorry, your registration failed. Please go back and try again.";
                }
            }

        } 
        else 
        {    
            $this->errors[] = "An unknown error occured.";   
        }   
    }

}

?>
