<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/object/AbstractObject.php');

class Store extends AbstractObject
{
	protected $name;

	public function __construct()
	{
		parent::__construct();

		$this->name = "";
	}
	
	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function toAssociativeArray()
	{	
		$employee = array(
			array('name', $this->name, "string"), 
	 	);

		if ( !($this->getId() == 0) ) {
			$store[] = array('store_id', $this->getId(), "integer");
		}

		return $store;
	}
}

?>
