<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/object/AbstractObject.php');

class CategoryProduct extends AbstractObject
{
	protected $description;
	
	public function __construct()
	{
		parent::__construct();

		$this->description	= "";
	}
	
	public function getDescription()
	{
		return $this->description;
	}

	public function setDescription( $description )
	{
		$this->description = $description;
		
	}

	public function toAssociativeArray()
	{	
		$category = array(
			array('description', $this->description, "string"),
	 	);	

		if ( !($this->getId() == 0) ) {
			$category[] = array('category_product_id',  $this->getId(),   "integer");
		}

		return $category;
	}
}
?>
