<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/includes/MySql.php');

class Login {

	protected $db_connection;
	
    protected $email;
	protected $password;
    protected $role_id;
    
    public $isUserLoggedIn;
    
    public  $errors; 	
    public  $messages;

	public function __construct() 
	{    
		$this->db_connection   = new MySql();
		$this->db_connection->dbConnect();
		
		$this->email 		    = "";
		$this->password 		= "";
	    $this->role_id          = 0;

        $this->isUserLoggedIn   = false;                  
	    
	    $this->errors 		    = array();                
	    $this->messages 		= array();                

        session_start();                                        
    
        if ( isset($_GET["logout"]) ) 
        {
            $this->logout();
        }

        if ( isset($_POST["logout"]) ) 
        {
            $this->logout();
		} 
		else if ( isset($_POST["login"]) ) 
		{
			$this->loginWithPostData();     
        } 
        else if ( !empty($_SESSION['email']) && ($_SESSION['isUserLoggedIn'] == 1) ) 
        {
            $this->loginWithSessionData();                
        }        
    }  
 	
 	private function loginWithSessionData() 
 	{    
 		// This method is called from the constructor where the user 
 		// was already logged-in (no need to do anything)
        $this->email    = $_SESSION['email'];
        $this->role_id  = $_SESSION['role_id'];
        $this->isUserLoggedIn = true;
        $this->messages[] = "You are logged in as ".$this->email;
    }

    private function loginWithPostData() 
    {    
		if ( !empty($_POST['email']) && !empty($_POST['password']) ) 
        {    
        	$this->email    = $_POST['email'];
        	$this->password = $_POST['password'];
            
            $query = "SELECT role_id, email, password FROM User WHERE email='$this->email';";
            $this->db_connection->selectFreeRun($query);
            $sqlResult = $this->db_connection->getObjects();

            if ( count($sqlResult) > 0 ) 
            {
                $user = $sqlResult[0];
                if ( $this->password == $user->password ) 
                {
                    // set the login status to true
                    $this->isUserLoggedIn = true; 
                    $this->messages[] = "You are logged in as ".$user->email;

                    // write user data into PHP SESSION 
                    // (a file on the server)
                    $_SESSION['email'] = $user->email;
                    $_SESSION['role_id'] = $user->role_id;
                    $_SESSION['isUserLoggedIn'] = 1; 
                } 
                else 
                {
                    $this->errors[] = "Wrong password. Try again.";
                }                

            } 
            else 
            {
                $this->errors[] = "User does not exist.";
            }

        } 
        else if ( empty($_POST['email']) ) 
        {
            $this->errors[] = "Email field was left empty.";
        } 
        else if ( empty($_POST['password']) ) 
        {
            $this->errors[] = "Password field was left empty.";
        }           
    }

 	public function logout() 
 	{
		$_SESSION = array();
        session_destroy();
        $this->isUserLoggedIn = false;
        $this->messages[] = "You have been logged out.";     
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getRoleId()
    {
        return $this->role_id;
    }

}

?>
