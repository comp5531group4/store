<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/object/AbstractObject.php');

class Purchase extends AbstractObject
{
 	protected $employee_id;
	protected $customer_id;
	protected $store_id;
	protected $purchase_date;
	protected $ship_date;

	public function __construct()
	{
		parent::__construct();

		$this->employee_id		= 0;
		$this->customer_id		= 0;
		$this->store_id			= 0;
		$this->purchase_date	= "";
		$this->ship_date		= "";
	}
	
	public function getEmployeeId()
	{
		return $this->employee_id;
	}

	public function setEmployee( $employee_id )
	{
		if ( is_numeric($employee_id) && $employee_id > 0 ) {
			$this->employee_id = $employee_id;	
		} else {
			$this->employee_id = 0;
		}
	}

	public function getCustomerId()
	{
		return $this->customer_id;
	}

	public function setCustomerId($customer_id)
	{
		if (is_numeric($customer_id) && $customer_id > 0) {
			$this->customer_id = $customer_id;	
		} else {
			$this->customer_id = 0;
		}
	}

	public function getStoreId()
	{
		return $this->store_id;
	}

	public function setStoreId($store_id)
	{
		if (is_numeric($store_id) && $store_id > 0) {
			$this->store_id = $store_id;	
		} else {
			$this->store_id = 0;
		}
	}

	public function getPurchaseDate()
	{
		return $this->purchase_date;
	}

	public function setPurchaseDate($purchase_date)
	{
		$this->purchase_date = $purchase_date;	
	}

	public function getShipDate()
	{
		return $this->ship_date;
	}

	public function setShipDate($ship_date)
	{
		$this->ship_date = $ship_date;	
	}

	public function toAssociativeArray()
	{	
		$purchase = array(
			array('employee_id', $this->employee_id, "integer"),
			array('customer_id', $this->customer_id, "integer"),
			array('store_id', $this->store_id, "integer"),
			array('purchase_date', $this->purchase_date, "date"),
			array('ship_date', $this->ship_date, "date")
	 	);	

		if ( !($this->getId() == 0) ) {
			$purchase[] = array('purchase_id',  $this->getId(),   "integer");
		}

		return $purchase;
	}
}
?>
