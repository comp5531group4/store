<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/object/AbstractObject.php');

class Service extends AbstractObject
{
 	protected $category_id;
 	protected $name; 
 	protected $price; 
 	protected $description;
 	protected $picture;
	
	public function __construct()
	{
		parent::__construct();
		$this->category_id	= 0;

		// type decimal
		$this->price 		= 0;

		// type: string
		$this->name 		= "";
		$this->description 	= "";
		$this->picture 		= "";
	}
	
	public function getCategoryId()
	{
		return $this->category_id;
	}

	public function setCategoryId($category_id)
	{
		$this->category_id = $category_id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function setPrice($price)
	{
		$this->price = $price;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getPicture()
	{
		return $this->picture;
	}

	public function setPicture($picture)
	{
		$this->picture = $picture;
	}

	public function toAssociativeArray()
	{	
		$service = array(
			array('category_id',  $this->category_id,  "integer"),
			array('name',  $this->name,  "string"),
			array('price',  $this->price,  "decimal"),
	 		array('description',  $this->description,  "string")
	 		array('picture',  $this->picture,  "string")
	 	);

		if ( !($this->getId() == 0) ) {
			$service[] = array('service_id',  $this->getId(),   "integer");
		}

		return $service;
	}
}
?>
