<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/object/AbstractObject.php');

include_once($_SERVER['DOCUMENT_ROOT'].'/mapper/MapperAddress.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/object/Address.php');

class Employee extends AbstractObject
{
 	protected $address_id;
 	protected $firstname;
 	protected $lastname; 
 	protected $commission;
 	protected $seniority;
 	protected $phone_number;
 	protected $gender;
 	protected $active;
	protected $hire_date;

	public function __construct()
	{
		parent::__construct();
		$this->address_id  	= 0;

		// type: string
		$this->firstname 	= "";
		$this->lastname 	= "";
		$this->seniority    = "";
		$this->phone_number = "";
		$this->gender 	  	= "";
		
		// type: integer
		$this->active 	  	= 1;

		// type: decimal
		$this->commission   = 0.50;
		
		// type: date
		$this->hire_date   = date("Y-m-d");
	}
	
	public function getAddress()
	{
		if ($this->address_id > 0)
		{
			$mapperAddress = new MapperAddress();
			return $mapperAddress->findById($this->address_id);
		} else {
			return new Address();
		}
	}

	public function getAddressId()
	{
		return $this->address_id;
	}

	public function setAddressId($address_id)
	{
		if (is_numeric($address_id) && $address_id > 0) {
			$this->address_id = $address_id;
		} else {
			$this->address_id = 0;
		}
	}

	public function getFirstname()
	{
		return $this->firstname;
	}

	public function setFirstname($fname)
	{
		$this->firstname = $fname;
	}

	public function getLastname()
	{
		return $this->lastname;
	}

	public function setLastname($lname)
	{
		$this->lastname = $lname;
	}

	public function getPhoneNumber()
	{
		return $this->phone_number;
	}

	public function setPhoneNumber($phone_number)
	{
		$this->phone_number = $phone_number;
	}

	public function getActive()
	{
		return $this->active ? true : false ;
	}

	public function setActive($active)
	{
		if ( $active ) {
			$this->active = 1;	
		} else {
			$this->active = 0;
		}
	}

	public function getCommission()
	{
		return $this->commission;
	}

	public function setCommission($commission)
	{
		$this->commission = $commission;
	}

	public function getSeniority()
	{
		return $this->seniority;
	}

	public function setSeniority($seniority)
	{
		$this->seniority = $seniority;
	}

	public function getGender()
	{
		return $this->gender;
	}

	public function setGender($gender)
	{
		$this->gender = $gender;
	}

	public function getHireDate()
	{
		return $this->hire_date;
	}

	public function setHireDate($hire_date)
	{
		$this->hire_date = $hire_date;
	}

	public function toAssociativeArray()
	{	
		$employee = array(
	 		array('address_id',   $this->address_id,  	"integer"),
			array('firstname',    $this->firstname, 	"string"),
			array('lastname',     $this->lastname,  	"string"),
			array('phone_number', $this->phone_number,	"string"),
			array('active',  	  $this->active,  		"integer"),
			array('commission',   $this->commission,  	"integer"),
	 		array('seniority',    $this->seniority,  	"string"),
	 		array('gender',  	  $this->gender,  		"string"),
	 		array('hire_date',    $this->hire_date,  	"string")
	 	);

		if ( !($this->getId() == 0) ) {
			$employee[] = array('employee_id', $this->getId(), "integer");
		}

		return $employee;
	}
}

?>
