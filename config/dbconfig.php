<?php
/* dbconfig.php
 * A class that contains information on the server 
 * and database used in the web application. 
 */
class DbConfig 
{    
    protected $serverName;
    protected $username;
    protected $password;
    protected $dbName;

    public function __construct()
    {
        // Concordia
        /*
        $this->serverName = 'clipper.encs.concordia.ca';
        $this->username   = 'jxc55311';                 
        $this->password   = 'jump1326';                 
        $this->dbName     = 'jxc55311';
        $this->path       = '';
        */
        
        // Local
        $this->serverName = 'localhost';
        $this->username   = 'root';
        $this->password   = 'root';
        $this->dbName     = 'project';
        
    }

    public function getParameters()
    {
        return $this->serverName." ".$this->username." ".$this->password." ".$this->dbName;
    }
}
?>
