$(document).ready(function(){
    $("#country").change(function () {
        $("#state, #city").find("option:gt(0)").remove();
        $("#state").find("option:first").text("Loading...");
        $.getJSON("/view/get/states.php", {
            country_id: $(this).val()
        }, function (json) {
            $("#state").find("option:first").text("Select State");
            for (var i = 0; i < json.length; i++) {
                $("<option/>").attr("value", json[i].state_id).text(json[i].name).appendTo($("#state"));
            }
        });
    });

    $("#state").change(function () {
        $("#city").find("option:gt(0)").remove();
        $("#city").find("option:first").text("Loading...");
        $.getJSON("/view/get/cities.php", {
            state_id: $(this).val()
        }, function (json) {
            $("#city").find("option:first").text("Select City");
            for (var i = 0; i < json.length; i++) {
                $("<option/>").attr("value", json[i].city_id).text(json[i].name).appendTo($("#city"));
            }
        });
    });

}); 
